# PyTorch 0.4.1 Anaconda3 5.2.0 (Python 3.6.5)
import numpy as np
import torch as T
import random
import datetime
import os


class Batch:
  def __init__(self, num_items, bat_size, seed=0):
    self.num_items = num_items; self.bat_size = bat_size
    self.rnd = np.random.RandomState(seed)
  def next_batch(self):
    return self.rnd.choice(self.num_items, self.bat_size, replace=False)

class Net(T.nn.Module):
  def __init__(self, hiddenLayerSize):
    super(Net, self).__init__()
    self.fc1 = T.nn.Linear(2, hiddenLayerSize)
    T.nn.init.xavier_uniform_(self.fc1.weight)  # glorot
    T.nn.init.zeros_(self.fc1.bias)
    self.fc2 = T.nn.Linear(hiddenLayerSize, 1)
    T.nn.init.xavier_uniform_(self.fc2.weight)
    T.nn.init.zeros_(self.fc2.bias)
  def forward(self, x):
    z = T.tanh(self.fc1(x))
    z = self.fc2(z)
    return z

class Scaler:
  def __init__(self, minScaledValue, maxScaledValue, minValue, maxValue):
    self.minScaledValue = minScaledValue
    self.maxScaledValue = maxScaledValue
    self.minValue = minValue
    self.maxValue = maxValue
  def scaleData(self, idata):
    l = self.maxScaledValue - self.minScaledValue
    return((idata - self.minValue) / (self.maxValue - self.minValue) * l +self.minScaledValue)
  def rescaleData(self, idata):
    l = self.maxValue - self.minValue
    return((idata - self.minScaledValue)/(self.maxScaledValue - self.minScaledValue) * l + self.minValue)

def evaluate(model, loss_func, data_x, data_y):
  X = T.Tensor(data_x)
  Y = T.FloatTensor(data_y)
  oupt = model(X)
  loss_obj = loss_func(oupt, Y)
  return loss_obj

def FullEval(model, loss_func, data_x, data_y, scale: Scaler):
  model.eval()
  X = T.Tensor(data_x)
  Y = T.FloatTensor(data_y)
  oupt = model(X)
  scaleY = scale.rescaleData(Y)
  scaleP = scale.rescaleData(oupt.data)
  scaleLoss = loss_func(scaleP, scaleY)
  loss_obj = loss_func(oupt, Y)
  (_, arg_maxs) = T.max(scaleP, dim=1)
  num_correct = T.sum(scaleY==arg_maxs.type(T.FloatTensor).round())
  acc = num_correct / T.numel(Y)
  model.train()
  return (acc, loss_obj, scaleLoss)

def printIteration(i, result):
  myprint("iteration = %4d" % i, end="")
  myprint("  loss = %7.4f" % result.item())

def printFinal(h, result):
  myprint("  loss = %7.4f" % result[1].item(), end="")
  myprint("  loss = %7.4f" % result[2].item(), end="")
  myprint("  accuracy = %0.2f%%" % result[0])

def generateSamples(n):
  result = []
  for i in range(0,n):
    result.append(random.sample(range(-10000,10000), 2))
  return result

def generateExpectedOutput(x):
  result = []
  for i in x:
    result.append([i[0] + i[1]])
  return np.array(result).astype(float)

def generateNumericAddition(n):
  train_x = generateSamples(n)
  train_y = generateExpectedOutput(train_x)
  train_x = np.array(train_x).astype(float)
  test_x = generateSamples(n)
  test_y = generateExpectedOutput(test_x)
  test_x = np.array(test_x).astype(float)
  return (train_x, train_y, test_x, test_y)

def accuracy(outputs, labels):
  _, preds = T.max(outputs, dim=1)
  return T.sum(preds == labels).item() /len(preds)

def TrainNetwork(hiddenLayerSize, train_x, train_y, test_x, test_y, scale: Scaler, max_i, b_denom, lr):
  net = Net(hiddenLayerSize)
  net = net.train() # set training mode
  lrn_rate = lr
  b_size = int(len(train_y)/b_denom)
  n_items = len(train_x)
  loss_func = T.nn.L1Loss()
  optimizer = T.optim.SGD(net.parameters(), lr=lrn_rate, momentum = 0.9)
  batcher = Batch(num_items=n_items, bat_size=b_size)
  best = 30000.0
  bestiteration = 0
  i = 0
  stop = False
  while (stop == False):
    curr_bat = batcher.next_batch()
    optimizer.zero_grad()
    result = evaluate(net, loss_func, train_x[curr_bat], train_y[curr_bat])
    if (result.item() < best and i >50000):
      best = result.item()
      bestiteration = i
      testResult = FullEval(net, loss_func, test_x, test_y, scale)
      myprint(str(i) + " - New Best Trn: " + str(result.item()) + " - Enc Test: " + str(testResult[1].item()) + " - Test: " + str(testResult[2].item()))
    if (result.item() == 0):
      best = result.item()
      bestiteration = i
      testResult = FullEval(net, loss_func, test_x, test_y, scale)
      stop = True
      myprint("interpolation")
    if (i % 50000 == 0): 
      myprint(".",end="")
    #if (i >= max_i): 
    #  print("max i hit: " + str(max_i))
    #  stop = True
    result.backward()
    optimizer.step()
    i = i + 1
  myprint("Best iteration: " + str(bestiteration) + " - best loss: " + str(best))
  result = evaluate(net, loss_func, train_x, train_y)
  printIteration(i, result)
  myprint("Training complete \n")
  net = net.eval()  # set eval mode
  myprint("Final Results for hidden Layer: " + str(hiddenLayerSize))
  printFinal(hiddenLayerSize, testResult)

def myprint(message, end = os.linesep):
    print(message, end)
    f = open("pyLog.txt", "a")
    message = message + end
    f.write(message)
    f.close()

myprint("\nBegin PyTorch Execution \n")
T.manual_seed(1)
np.random.seed(1)
n = 100
b_denom = 2
lr = 0.00001
max_i = 150000000
myprint("Generating and loading data into memory \n")
data = generateNumericAddition(n)
scale = Scaler(-1,1, data[1].min(), data[1].max())
sd0 = scale.scaleData(data[0])
sd1 = scale.scaleData(data[1])
sd2 = scale.scaleData(data[2])
sd3 = scale.scaleData(data[3])
f = open("pyLog.txt", "w")
f.close()
myprint("Starting training")
myprint("Sample Size: " + str(n))
myprint("Max i: " + str(max_i))
myprint("M: 0.9")
i = 80
while (i < 500):
  myprint("LR: " + str(lr))
  myprint(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
  myprint("hiddenLayer: " + str(i))
  TrainNetwork(i, sd0,sd1,sd2,sd3, scale, max_i, b_denom, lr)
  i = i + 5